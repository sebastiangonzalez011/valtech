module.exports = {
    API_URL: {
        'titanic': 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=titanic-passengers&q=&rows=240&facet=survived&facet=pclass&facet=sex&facet=age&facet=embarked&refine.survived=Yes'
    }
}